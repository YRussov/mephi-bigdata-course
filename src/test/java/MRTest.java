import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MRTest {

    private static final Integer min_length = 7;
    private static final Counter counter = new Counters.Counter();
    static {
        counter.setValue(0);
    }


    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;
    List<Text> fileByString = new ArrayList<>();

    @Before
    public void setUp() {
        File file = new File("src/test/resources/TestFile.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            for(String line; (line = br.readLine()) != null; ) {
                fileByString.add(new Text(line));
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading file", e);
        }
        MyMapper mapper = new MyMapper();
        MyReducer reducer = new MyReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() {

        String longest = "looooooooooooooooooooooooooooooooooooooooooooooooooooooooooong";
        StringJoiner stringJoiner = new StringJoiner();
        for (Text text : fileByString) {
            stringJoiner.append(text.toString());
        }
        mapDriver.withInput(new LongWritable(), new Text(stringJoiner.build()));
        mapDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() {
        String longest = "looooooooooooooooooooooooooooooooooooooooooooooooooooooooooong";
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(longest.length()));
        reduceDriver.withInput(new Text(longest), values);
        reduceDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() {
        String longest = "looooooooooooooooooooooooooooooooooooooooooooooooooooooooooong";
        StringJoiner stringJoiner = new StringJoiner();
        for (Text text : fileByString) {
            stringJoiner.append(text.toString());
        }
        mapReduceDriver.withInput(new LongWritable(), new Text(stringJoiner.build()));
        mapReduceDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        mapReduceDriver.runTest();
    }

    static class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        
	//Writable entires are reused
	private final Text outputKey = new Text();
	private final IntWritable outputValue = new IntWritable();

	//Longest word
	private String longWord;

	//Longest word initialization
	@Override
        protected void setup(Context context) throws java.io.IOException, InterruptedException {
            longWord = "";
        }


        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws java.io.IOException, InterruptedException {

            StringTokenizer itr = new StringTokenizer(value.toString());
            
		//Search for longest word. fist it was just ""
            while (itr.hasMoreTokens()) {
                String temp = itr.nextToken();
                if(temp.length() < min_length) {
                    counter.increment(1);
                } else if (temp.length() > longWord.length()) {
					longWord = temp;
				}
            }
	}

	@Override
        protected void cleanup(Context context) throws IOException, InterruptedException {

		//Writable entires are reused
		//Here we write the longest word and it's length
		outputKey.set(longWord);
		outputValue.set(longWord.length());
		context.write(outputKey, outputValue);
        }


    }

	// Reducer key = word,value = length of the word
    static class MyReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {
	
	//Writable entires are reused
	private final IntWritable outputVal = new IntWritable();

        protected void reduce(Text key, Iterable<Integer> values, Context context) throws java.io.IOException, InterruptedException {
		//Writable entires are reused
	outputVal.set(key.getLength());
	context.write(key, outputVal);
        }
    }

}

class StringJoiner {

    private String result;

    private String delimiter;

    private String prefix;

    private String suffix;

    public StringJoiner setDelimiter(String delimiter) {
        this.delimiter = delimiter;
        return this;
    }

    public StringJoiner setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public StringJoiner setSuffix(String suffix) {
        this.suffix = suffix;
        return this;
    }

    public List<String> stringList = new ArrayList<>();

    public StringJoiner() {
        delimiter = " ";
        prefix = "";
        suffix = "";
        result = "";
    }

    public StringJoiner append(String string) {
        stringList.add(string);
        return this;
    }

    public String build() {

        result = suffix;
        for (String string: stringList) {
            addBuildChain(string);
        }
        result += prefix;

        return result;
    }

    private void addBuildChain(String string) {
        result += string + delimiter;
    }
}