package com.YRussov.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

    static class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        
	//Writable entires are reused
	private final Text outputKey = new Text();
	private final IntWritable outputValue = new IntWritable();

	//Longest word
	private String longWord;

	//Longest word initialization
	@Override
        protected void setup(Context context) throws java.io.IOException, InterruptedException {
            longWord = "";
        }


        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws java.io.IOException, InterruptedException {

            StringTokenizer itr = new StringTokenizer(value.toString());
            
		//Search for longest word. fist it was just ""
            while (itr.hasMoreTokens()) {
                String temp = itr.nextToken();
                if(temp.length() > longWord.length()) {
                    longWord = temp;
                }
            }
	}

	@Override
        protected void cleanup(Context context) throws IOException, InterruptedException {

		//Writable entires are reused
		//Here we wrie the longest word and it's length
		outputKey.set(longWord);
		outputValue.set(longWord.length());
		context.write(outputKey, outputValue);
        }


    }

	// Reducer key = word,value = length of the word
    static class MyReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {
	
	//Writable entires are reused
	private final IntWritable outputVal = new IntWritable();

        protected void reduce(Text key, Iterable<Integer> values, Context context) throws java.io.IOException, InterruptedException {
		//Writable entires are reused
	outputVal.set(key.getLength());
	context.write(key, outputVal);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapred.textoutputformat.separatorText", ",");
        Job job = Job.getInstance(conf, "word length count");
        job.setJarByClass(Main.class);
        job.setMapperClass(MyMapper.class);
        job.setCombinerClass(MyReducer.class);
        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/IniText1.txt"));
        FileOutputFormat.setOutputPath(job, new Path("hdfs://localhost/user/cloudera/result15"));
        //FileInputFormat.addInputPath(job, new Path(args[0])
	//FileInputFormat.addInputPath(job, new Path(args[1]);
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

